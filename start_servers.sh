#!/bin/bash

mkdir -p xcache/admin
mkdir -p xcache/localroot

mkdir -p xrootd/admin
mkdir -p xrootd/localroot

cat > xrootd.cfg <<EOF
xrd.allow host localhost
oss.localroot  $(pwd)/xrootd/localroot
all.adminpath $(pwd)/xrootd/adminpath
all.export / stage r/o
EOF

cat > xcache.cfg <<EOF
xrd.allow host localhost
oss.localroot $(pwd)/xcache/localroot
all.adminpath $(pwd)/xcache/admin
ofs.osslib  /usr/lib64/libXrdPss.so
pss.cachelib libXrdPfc.so
pss.origin =
all.export /root:/ stage r/o
pfc.prefetch 0
EOF

xrootd -b -l xrootd/log -s xrootd/pid -c xrootd.cfg -p 1094 -d
xrootd -b -l xcache/log -s xcache/pid -c xcache.cfg -p 1095 -d
