# Xcache local testing

To start the servers run

```
./start_servers.sh
```

By default you'll have an xrootd sever listening on `localhost:1094` and an xcache server on `localhost:1095`. Config files are generated on-the-fly, so edit  `start_servers.sh` for experimenting with configs.
The local xrootd server will serve files that you copy or symlink into `xrootd/localroot`.

For example, put random data in there:
```
dd if=/dev/urandom of=xrootd/localroot/random_file bs=1M count=100
```
Which can be accessed via
```
root://localhost:1094//random_file
```
Or also via Xcache
```
root://localhost:1095//root://localhost:1094//random_file
```

To stop them and clean the temporary directories and files run
```
./stop_servers && ./clean.sh
```
